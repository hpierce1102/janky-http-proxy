janky-http-proxy
================

A terrible HTTP forward proxy. 

Why...?
-------

I have an AWS Lambda function that I need to connect to the internet and to a VPC. While the "correct" solution involves
provisioning a NAT in the VPC for this, NATs cost like $30/month on AWS and I don't really need anything fancy. Instead of
the NAT, this server runs on an EC2 instance in the VPC so that the Lambda function can connect to this server and use
the EC2 instance's IP address to access the internet.

How
---

```
npx @haydenpierce/janky-http-proxy --port=3000

POST localhost:3000
{
    "method": "GET",
    "hostname": "www.example.com",
    "path": "/path-to-whatever"
}
```