#!/usr/bin/env node

const getServer = require('../src/getServer.js');
const Yargs = require('yargs');

const argv = Yargs(process.argv.slice(2))
    .describe('port', 'The port for the server to listen on.')
    .default({
        'port': '3000'
    })
    .argv;


const app = getServer();

app.listen(argv.port, () => {
    console.log(`Example app listening on port ${argv.port}`)
});

