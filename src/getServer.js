const express = require('express')
const app = express()

function getServer() {
    app.use('/', require('./index/router'))

    return app;
}

module.exports = getServer;