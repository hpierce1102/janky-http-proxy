var express = require('express');
var axios = require('axios');
const https = require("https");

var index = express.Router();


function sendRequestHttps(options, dataBuffer) {
    return new Promise((resolve, reject) => {
        var req = https.request(options, function (res) {
            var chunks = [];

            res.on("data", function (chunk) {
                chunks.push(chunk);
            });

            res.on("end", function (chunk) {
                var body = Buffer.concat(chunks);
                resolve({
                    statusCode: res.statusCode,
                    body: body
                });
            });


            res.on("error", function (error) {
                console.log('error');
                console.log(e);
                reject(error);
            });
        });

        req.on('error', (e) => {
            console.log('error');
            console.log(e);
            reject(e);
        })

        if (typeof dataBuffer !== 'undefined') {
            req.write(dataBuffer);
        }

        req.end();
    })
}



index.use(express.json());

index.post('/', function(req, res) {
    const errors = [];
    const allowedMethods = ['GET'];
    if (!allowedMethods.includes(req?.body?.method)) {
        errors.push(`method '${req?.body?.method}' is not allowed. Allowed methods are ${allowedMethods.join(',')}.`)
    }

    console.log(req.body);

    if (req?.body?.path === null) {
        errors.push(`path is required.`)
    }

    if (req?.body?.hostname === null) {
        errors.push(`hostname is required.`)
    }

    if (errors.length > 0) {
        res.status(400);
        res.set('X-PROXY-ERRORS', '1');
        res.set('Content-Type', 'application/json');
        res.send(JSON.stringify({
            errors
        }));
        console.log(errors);
        return;
    }

    switch (req?.body?.method) {
        case 'GET':
            // axios.get('https://' + req?.body?.url)
            sendRequestHttps({
                'method': 'GET',
                'hostname': req.body.hostname,
                'path': req.body.path,
            })
                .then((response) => {
                    res.status(200);
                    res.set('X-PROXY-HEADERS', JSON.stringify(response.headers));
                    res.set(response.headers);
                    res.send(Buffer.from(response.body));
                })
                .catch((response) => {
                    res.status(response.status);
                    res.set('X-PROXY-HEADERS', JSON.stringify(response.headers));
                    res.send(response.data);
                })
            break;
        default:
            res.status(500);
            res.set('Content-Type', 'application/json');
            res.send(JSON.stringify({
                errors: [`${req?.body?.method} not implemented.`]
            }));

    }
});

module.exports = index;